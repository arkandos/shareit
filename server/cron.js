const fs = require('fs')
const cron = require('node-cron')
const path = require('path')

const db = require('./database')
const config = require('./config')


module.exports = startCronjobs;


function startCronjobs() {
    // Clear all old files at 2:00 AM
    const deleteTask = cron.schedule(config.deleteCrontab, deleteOldFiles)
    const purgeTask = cron.schedule(config.purgeCrontab, purgeDeletedDocuments)

    return stopCronjobs;


    function stopCronjobs() {
        deleteTask.stop()
        purgeTask.stop()
    }
}


// flag to not execute this function more than once at the same time.
let deletingOldFiles = false;
async function deleteOldFiles() {
    if (deletingOldFiles)  return;
    deletingOldFiles = true;

    const beforeDate = new Date(Date.now() - config.maxFileAge)

    console.log("Removing files older than", beforeDate)

    const files =
        await db.getMetaFilesBefore(beforeDate)
            .catch(logDefault('Could not get all files from database', []))

    const deletedFiles =
        await Promise.all(
            files.map(file =>
                new Promise(function (resolve, reject) {
                    fs.unlink(file.path, err => err ? reject(err) : resolve(file))
                }).catch(logDefault('Coult not remove file from disk', false))))

    await db.deleteMetaFiles(deletedFiles.filter(Boolean))
        .catch(logDefault('Could not remove files from database: ', false));

    deletingOldFiles = false;
}


async function purgeDeletedDocuments() {
    const beforeDate = new Date(Date.now() - config.maxDeletedAge)

    await db.purgeDeletedDocuments(beforeDate).catch(console.error)
}


function logDefault(msg, defaultValue) {
    return function _catch(err) {
        console.error(msg, ': ', err)
        return defaultValue
    }
}
