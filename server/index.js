const express = require('express')
const throttle = require('express-throttle')
const path = require('path')

const _omit = require('lodash.omit')

const config = require('./config')
const startCronjobs = require('./cron')
const upload = require('./upload')
const db = require('./database')


const pathPublic = config.publicPath


const app = express()

// API

// Throttle uploads to a moving window of 10 files per day
app.post('/file', throttle({ rate: config.requestRate }), upload.single('file'), A(uploadFile))
app.get('/file/:id', A(getMetaFile))
app.get('/download/:id', A(downloadFile))
app.get('/download/:filename/:originalname', A(downloadFile))

// Frontend Routes
// These need to be after the API routes, such that '/file' does not match '/:id'

app.use(express.static(pathPublic))
app.get('/:id', serveFrontend)
app.get('/:id/:originalname', serveFrontend)


// add error handler after all routes to make sure all errors end up here
// express checks all handlers in order, so the error handler always has to be last,
// so all errors in previous handlers (middleware) will be handled.
app.use(notFoundHandler);
app.use(errorHandler);

db.upgradeDatabase().then(function(latestRevision) {
    console.log("Database upgraded to rev", latestRevision)

    startCronjobs();

    app.listen(config.port, function () {
        console.log(`Listening on ${config.port}`)
    })
}).catch(console.error)


/**
 * Upload a file to the server, and store some meta information about it.
 * The returned JSON will be that meta information of that file.
 */
async function uploadFile(req, res) {
    if (!req.file) {
        throw ({ error: 'missing', field: 'file', msg: 'No file uploaded' })
    }

    const metafile = await db.createMetaFile(req.file);
    return res.json(omitHiddenFields(metafile));
}


/**
 * Get the meta information of a file.
 * This is used by the frontend to display properties of
 * a file, so the reciever can make sure they actually want it.
 */
async function getMetaFile(req, res) {
    const id = required(req.params.id, 'id', 'file id')

    const metafile = await db.getMetaFileById(id)

    return res.json(omitHiddenFields(metafile))
}


/**
 * Actually download a file.
 * Uses the mime type and the file size of the meta
 * information stored on upload, instead of letting express
 * guess the right things.
 */
async function downloadFile(req, res) {
    const filename = required(req.params.filename, 'filename', 'filename')

    const metafile = await db.getMetaFileByFileName(filename)

    return res.download(metafile.path, metafile.filename, {
        headers: {
            'Content-Type': metafile.mime,
            'Content-Length': metafile.size,
            'X-Timestamp': metafile.created
        }
    })
}


/**
 * The frontend has its own routing - so most requests to the frontend
 * just load `index.html` and let the frontend router take over.
 */
async function serveFrontend(req, res, next) {
    const id = req.params.id
    const filename = req.params.filename

    return res.sendFile('index.html', {
        root: pathPublic
    })
}


/**
 * Throw an error on XHR requests (delegating the display of the 404 using the default error handler),
 * or load the frontend on all other requests to make a user-friendly error page.
 */
function notFoundHandler(req, res, next) {
    if (req.xhr) {
        throw ({
            error: 'notFound',
            status: 404,
            path: req.path,
            method: req.method,
            msg: `'Cannot ${req.method} '${req.path}'`
        })
    } else {
        // not a XHR request - display a end-user friendly message using the frontend routing
        res.status(404)
        serveFrontend(req, res, next)
    }
}


/**
 * Convert objects and standard Errors into JSON objects.
 * All frontend routes are just static files, so we don't handle those
 * errors properly for users, they get to enjoy the JSON too.
 */
function errorHandler(err, req, res, next) {
    if (res.headersSent) {
        return next(err)
    }

    // an error with this code gets thrown by multer.
    if (err.code == 'LIMIT_FILE_SIZE') {
        err = ({
            error: 'fileToBig',
            status: 413,
            limit: config.maxFileSize,
            msg: "The file you tried to upload is too big."
        });
    }
    else if (err instanceof Error) {
        err = ({
            error: 'unexpectedException',
            code: err.code,
            name: err.constructor.name,
            msg: err.message
        })
    }

    res.status(err.status || 500).json(err)
}


function omitHiddenFields(metafile) {
    return _omit(metafile, 'path', 'type', 'deleted', '_rev')
}


/**
 * Wrap a Promise-returning route handler such that the errors returned in the
 * Promise will be captured and routed through express.
 */
function A(fn) {
    return function asyncMiddleware(req, res, next) {
        return Promise.resolve()
            .then(() => fn(req, res, next))
            .catch(next)
    }
}


/**
 * Check if `value` is truthy, and return it.
 * If it's not, throw a "missing field" error instead.
 */
function required(value, fieldname, label) {
    if (!value) {
        throw ({ error: 'missing', field: fieldname, msg: `No ${label} specified` })
    }

    return value
}
