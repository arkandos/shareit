const { pseudoRandomBytes } = require('crypto')
const { mkdir } = require('fs')
const mkdirp = require('mkdirp')
const multer = require('multer')
const path = require('path')

const config = require('./config')

const pathDestination = config.filesPath

// flag to only check if the `pathDestination` exists once.
let destinationCreated = false


/**
 * Similar to the default configuration, creates a random 16 byte hex
 * filename for all files, but also uses the first byte as a sub
 * directory inside `pathDestination`. This improves file system lookup
 * speeds later when we download things again.
 */
const upload = multer({
    limits: {
        fileSize: config.maxFileSize,
        // fieldSize: config.maxFileSize
    },

    storage: multer.diskStorage({
        destination(req, file, cb) {
            if (destinationCreated) {
                cb(null, pathDestination)
            } else {
                mkdirp(pathDestination, function (err, made) {
                    destinationCreated = true
                    cb(err, pathDestination)
                })
            }
        },

        filename(req, file, cb) {
            pseudoRandomBytes(16, function (err, raw) {
                if (err) return cb(err)

                const id = raw.toString('hex')

                const subdir = id.substr(0, 2)

                mkdir(path.join(pathDestination, subdir), function (err) {
                    if (err && err.code != 'EEXIST') return cb(err)

                    cb(null, path.join(subdir, id))
                })
            })
        }
    })
})


module.exports = upload
