const path = require('path')

const _pickBy = require('lodash.pickby')


const defaults = {
    port: 8080,
    // THIS IS IN MS, NOT SECONDS!
    maxFileAge: 3 * 24 * 3600 * 1000,
    maxDeletedAge: 28 * 24 * 3600 * 1000,
    maxFileSize: 1024 * 1024 * 1024, // 1GB
    deleteCrontab: '0 2 * * *',
    purgeCrontab: '0 4 * * *',
    requestRate: '10/day',
    dataPath: path.resolve(__dirname, '..', 'data'),
    filesPath: path.resolve(__dirname, '..', 'data', 'files'),
    publicPath: path.resolve(__dirname, '..', 'public')
}

const config = {
    port: parseInt(process.env.PORT),
    maxFileAge: parseInt(process.env.APP__MAX_FILE_AGE),
    maxDeletedAge: parseInt(process.env.APP__MAX_DELETED_AGE),
    maxFileSize: parseInt(process.env.APP__MAX_FILE_SIZE),
    deleteCrontab: process.env.APP__DELETE_CRONTAB,
    purgeCrontab: process.env.APP_PURGE_CRONTAB,
    requestRate: process.env.APP__REQUEST_RATE,
    dataPath: process.env.APP__DATA_PATH,
    filesPath: process.env.APP__FILES_PATH,
    publicPath: process.env.APP__PUBLIC_PATH
}

const configWithDefaults = Object.assign({}, defaults, _pickBy(config, v => v !== undefined && v !== null && !Number.isNaN(v)))

console.log("Config:", JSON.stringify(configWithDefaults))

module.exports = configWithDefaults

