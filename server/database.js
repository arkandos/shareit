const { pseudoRandomBytes } = require('crypto')
const path = require('path')
const PouchDB = require('pouchdb')

const config = require('./config')


/**
 * @Volatile
 * Incrementing value storing the latest revision of our database model.
 * This number is compared to the one stored in the database at startup, to
 * (optionally) run any upgrades required, installing new views, etc.
 */
const currentRevision = 3


const db = new PouchDB(config.dataPath)


/**
 * Upgrade the database to the newest version.
 */
async function upgradeDatabase() {
    const revisionDoc = await db.get('revision').catch(err => ({ _id: 'revision', revision: 0 }))
    const onlineRevision = revisionDoc.revision

    if (onlineRevision < currentRevision) {
        await upgradeDatabaseHelper(onlineRevision, currentRevision)
        await db.put({ ...revisionDoc, revision: currentRevision })
    }

    return currentRevision
}


/**
 * Extract meta information about a file from a Multer file object,
 * storing it into the database.
 *
 * The filename will be used as an ID, so you need to make sure the
 * filename is unique, or this function will fail.
 *
 * @returns {Promise} A promise resolving to the inserted document
 */
async function createMetaFile(file) {
    const now = Date.now()

    const id = await generateId()

    const doc = {
        _id: id,
        type: 'file',
        fileId: path.basename(file.filename),

        path: file.path,

        filename: file.originalname,
        encoding: file.encoding,
        mime: file.mimetype,
        size: file.size,

        created: now
    }

    const res = await db.put(doc)

    if (!res.ok) {
        throw ({ error: 'exist', msg: `File '${doc._id}' already exists` })
    }

    return doc
}


/**
 * Queries the database for the meta information for the file
 * given the meta file id.
 *
 * @returns {Promise} A promise resolving to a file meta document for that meta file
 */
async function getMetaFileById(id) {
    const _notFound = ({ error: 'notFound', status: 404, file: id, msg: `File '${id}' not found` })

    const doc = await db.get(id).catch(err => { throw _notFound })

    if (!doc || doc.type != 'file') {
        throw _notFound
    }


    // db.get uses the allDocs index, which includes deleted files,
    // so we have to check manually here.
    //
    // Instead of the usual _notFound error, we throw a `410 Gone` error here,
    // to make it easier for clients to distinquish the 2 possibilities.
    if (doc.deleted) {
        throw ({ error: 'gone', status: 410, file: id, msg: `The file that was ${id} got since then deleted.` })
    }

    return doc
}


/**
 * Query a file by its download file name. This filename is also an id that looks
 * a lot like a meta file id, but is intentionally different to make it harder to guess the
 * download id from a file id.
 */
async function getMetaFileByFileName(filename) {
    const _notFound = ({ error: 'notFound', status: 404, file: filename, msg: `File '${filename}' not found` })

    const res = await db.query('file/fileId', {
        include_docs: true,
        key: filename
    })

    if (res.rows.length != 1) {
        throw _notFound
    }

    const doc = res.rows[0].doc

    if (doc.type != 'file') {
        throw _notFound
    }

    return doc
}


/**
 * Get files that where created before some given date.
 * This is usefull for garbage collection, in combination with deleteMetaFiles
 *
 * @param {Date} dateBefore The latest date acceptable.
 */
async function getMetaFilesBefore(dateBefore) {
    const key = dateBefore.getTime()

    const result = await db.query('file/created', {
        descending: true,
        startkey: key,
        include_docs: true,
        reduce: false,
    })

    return result.rows.map(row => row.doc)
}


/**
 * Delete some metafile entries from the database.
 * Instead of actually doing a delete, this just sets a "soft-delete" flag
 * on the document. All other views and query function in this module
 * check this flag before returning a document.
 */
async function deleteMetaFiles(docs) {
    if (docs.length) {
        const result = await db.bulkDocs(
            docs.map(doc => ({
                ...doc,
                deleted: true
            })))

        const failed = result.filter(row => row.error)

        if (failed.length) {
            throw failed
        }
    }
}


/**
 * Finally remove all soft-deleted document for real, this time.
 */
async function purgeDeletedDocuments(dateBefore) {
    const key = dateBefore.getTime()
    const res = await db.query('deleted/deleted', {
        descending: true,
        startkey: key,
        include_docs: false,
        reduce: false
    })

    const result = await db.bulkDocs(
        res.rows.map(row => ({
            _id: row.id,
            _rev: row.value,
            _deleted: true
        }))
    )

    await db.viewCleanup()
    await db.compact()

    const failed = result.filter(row => row.error)

    if (failed.length) {
        throw failed
    }
}


module.exports = {
    createMetaFile,
    getMetaFileById,
    getMetaFileByFileName,
    getMetaFilesBefore,
    deleteMetaFiles,
    upgradeDatabase,
    purgeDeletedDocuments,
}


/**
 * Helper function for upgradeDatabase. This actually contains all the migrations.
 * This function could get quite large, so it is removed from the general logic / public
 * code to improve readability. Conceptually, there is nothing really interesting going in here.
 */
async function upgradeDatabaseHelper(onlineRevision, targetRevision) {
    if (onlineRevision < 1 && targetRevision < 3) { // upper bound: only need this if we install rev < 3
        await updateView('file/created', function (doc) {
            if (doc.type == 'file') {
                emit(doc.created, { _id: doc._id, _rev: doc._rev })
            }
        }, '_count')
    }

    if (onlineRevision < 2 && targetRevision < 3) { // upper bound: only need this if we install rev < 3
        await updateView('file/fileId', function (doc) {
            if (doc.type == 'file') {
                emit(doc.fileId)
            }
        })
    }

    if (onlineRevision < 3) {
        await updateView('file/created', function (doc) {
            if (doc.type == 'file' && !doc.deleted) {
                emit(doc.created)
            }
        }, '_count')
        await updateView('file/fileId', function (doc) {
            if (doc.type == 'file' && !doc.deleted) {
                emit(doc.fileId)
            }
        })
        await updateView('deleted/deleted', function (doc) {
            if (doc.deleted) {
                emit(doc.created, doc._rev)
            }
        }, '_count')
    }
}


/**
 * Helper function to create a new design doc, or add/update the view in an existing one.
 *
 * @param {string} viewIdentifier A string identifying the view, using the same syntax as PouchDB.prototype.query
 * @param {Function} map The map function.
 * @param {Function|string|undefined} reduce Optional reduce function, or a string to use one of the built-in ones.
 */
async function updateView(viewIdentifier, map, reduce) {
    const [designDocName, viewName] = viewIdentifier.split('/')

    const designDocId = `_design/${designDocName}`

    const existingDesignDoc = await db.get(designDocId)
        .catch(err => ({ _id: designDocId, views: {} }))

    const newDesignDoc = {
        ... existingDesignDoc,
        views: {
            ... existingDesignDoc.views,
            [viewName || designDocName]: {
                map: typeof map == 'function'
                    ? map.toString()
                    : map,

                reduce: typeof reduce == 'function'
                    ? reduce.toString()
                    : reduce
            }
        }
    }

    await db.put(newDesignDoc)
}


/**
 * Generate a random hex string.
 */
function generateId() {
    return new Promise(function (resolve, reject) {
        pseudoRandomBytes(16, function (err, buf) {
            if (err) return reject(err);
            else resolve(buf.toString('hex'))
        })
    })
}



