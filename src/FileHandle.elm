module FileHandle exposing (FileHandle, isDirectory, onDragEnter, onDragLeave, onDragOver, onDrop)

import Html exposing (Attribute)
import Html.Events exposing (custom, on)
import Json.Decode as D exposing (Decoder, Value)


type alias FileHandle =
    { name : String
    , size : Int
    , mime : String
    , handle : Value
    }


isDirectory : FileHandle -> Bool
isDirectory file =
    -- Firefox reports the size to be 0, while Chrome/Opera seems to be
    -- reporting the size of the directory on the disk, which afaik is
    -- 4096 all the time on NTFS at least?
    (file.size == 0 || file.size == 4096) && file.mime == ""


onDirect : String -> msg -> Attribute msg
onDirect eventName msg =
    let
        decoder =
            D.map2 (==)
                (D.field "currentTarget" D.value)
                (D.field "target" D.value)
                |> D.andThen
                    (\isDirect ->
                        if isDirect then
                            D.succeed
                                { message = msg
                                , preventDefault = True
                                , stopPropagation = True
                                }

                        else
                            D.fail "Event only fired on direct hits."
                    )
    in
    custom eventName decoder


onDragEnter : msg -> Attribute msg
onDragEnter =
    onDirect "dragenter"


onDragOver : msg -> Attribute msg
onDragOver =
    onDirect "dragover"


onDragLeave : msg -> Attribute msg
onDragLeave =
    onDirect "dragleave"



-- WARNING(jreusch): This event only works if you are also subscribed to onDragOver.
-- if you are not, preventDefault will not work, and the browser will still do
-- its default action for some reason.


onDrop : (FileHandle -> msg) -> Attribute msg
onDrop msg =
    let
        decoder =
            fileEventDecoder msg
                |> D.map
                    (\file ->
                        { message = file
                        , preventDefault = True
                        , stopPropagation = True
                        }
                    )
    in
    custom "drop" decoder


fileEventDecoder : (FileHandle -> msg) -> Decoder msg
fileEventDecoder msg =
    let
        file decoder =
            D.at [ "dataTransfer", "files", "0" ] decoder
    in
    D.map4 FileHandle
        (file <| D.field "name" D.string)
        (file <| D.field "size" D.int)
        (file <| D.field "type" D.string)
        (file <| D.value)
        |> D.map msg
