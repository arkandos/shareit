module Flags exposing (Flags, decode, store)

import Json.Decode as D exposing (Decoder)
import Json.Encode as E exposing (Value)
import Ports exposing (persistFlags)


type alias Flags =
    { uploadAccepted: Bool
    , downloadAccepted: Bool
    }


decode : Value -> Flags
decode jsonValue =
    case D.decodeValue decoder jsonValue of
        Ok flags ->
            flags

        Err _ ->
            defaultFlags


store : Flags -> Cmd msg
store flags =
    persistFlags (encode flags)


---
--- Internal
---


defaultFlags : Flags
defaultFlags =
    { uploadAccepted = False
    , downloadAccepted = False
    }


decoder : Decoder Flags
decoder =
    D.map2 Flags
        (D.field "uploadAccepted" D.bool)
        (D.field "downloadAccepted" D.bool)


encode : Flags -> Value
encode flags =
    E.object
        [ ( "uploadAccepted", E.bool flags.uploadAccepted )
        , ( "downloadAccepted", E.bool flags.downloadAccepted )
        ]
