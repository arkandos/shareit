module Filesize exposing (format)


type alias UnitDefinition =
    { factor : Int
    , suffix : String
    , decimalPlaces : Int
    }


unitList : List UnitDefinition
unitList =
    [ { factor = 1024 * 1024 * 1024, suffix = "GiB", decimalPlaces = 2 }
    , { factor = 1024 * 1024, suffix = "MiB", decimalPlaces = 1 }
    , { factor = 1024, suffix = "KiB", decimalPlaces = 1 }
    ]


defaultUnit : UnitDefinition
defaultUnit =
    { factor = 1, suffix = "B", decimalPlaces = 0 }


unitForSize : Int -> List UnitDefinition -> UnitDefinition
unitForSize size units =
    case units of
        [] ->
            defaultUnit

        head :: tail ->
            if head.factor < size then
                head

            else
                unitForSize size tail


format : Int -> String
format size =
    let
        ( absoluteSize, signFactor ) =
            if size < 0 then
                ( -1 * size, -1 )

            else
                ( size, 1 )

        unit =
            unitForSize absoluteSize unitList

        unitSize =
            toFloat absoluteSize / toFloat unit.factor

        roundedSize =
            signFactor * roundToDecimalPlaces unit.decimalPlaces unitSize
    in
    String.fromFloat roundedSize ++ " " ++ unit.suffix


roundToDecimalPlaces : Int -> Float -> Float
roundToDecimalPlaces decimalPlaces num =
    let
        factor =
            toFloat (10 ^ decimalPlaces)

        roundf =
            toFloat << round
    in
    if decimalPlaces == 0 then
        roundf num

    else
        roundf (num * factor) / factor
