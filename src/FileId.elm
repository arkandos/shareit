module FileId exposing (FileId, decoder, toString, urlParser)

import Json.Decode as Decoder exposing (Decoder)
import Url.Parser as Parser exposing (Parser)


type FileId
    = FileId String


toString : FileId -> String
toString (FileId internalId) =
    internalId


urlParser : Parser (FileId -> a) a
urlParser =
    Parser.map FileId Parser.string


decoder : Decoder FileId
decoder =
    Decoder.map FileId Decoder.string
