module Page.Error exposing (Error(..), Msg, update, view)

import Browser exposing (Document)
import Browser.Navigation as Nav
import FileId as FileId exposing (FileId)
import Filesize as Filesize
import Html exposing (..)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Route as Route exposing (Route(..))


type Error
    = UnexpectedServerError
    | DirectoryDropped
    | FileToBig { size : Int, max : Int }
    | QuotaReached { max : Int }
    | FileExpired FileId
    | FileNotFound FileId
    | NotFound


type Msg
    = GoHome


update : Nav.Key -> Msg -> Cmd msg
update key msg =
    case msg of
        GoHome ->
            Route.replaceUrl key Home


view : Error -> Document Msg
view error =
    Document "Oops :(" [ viewError error ]


viewError : Error -> Html Msg
viewError error =
    case error of
        UnexpectedServerError ->
            viewErrorMsg <| "An unexpected error has occurred! If you are sure that what you did should have worked, maybe try again?"

        DirectoryDropped ->
            viewErrorMsg <| "You tried to upload a directory instead of a file! Currently, only single files are supported. But you may upload an archive of that directory, instead!"

        FileToBig { size, max } ->
            viewErrorMsg <| "You tried to upload " ++ Filesize.format size ++ ", but we currently only allow files up to " ++ Filesize.format max ++ ". Maybe you can zip the file to make it smaller?"

        QuotaReached { max } ->
            viewErrorMsg <| "Unfortunately, we currently only allow up to " ++ String.fromInt max ++ " uploads per day. This is a moving limit though - you might be able to upload again in a few hours!"

        FileExpired id ->
            viewErrorMsg <| "This file already expired. Please ask the uploader to share it again!"

        FileNotFound id ->
            viewErrorMsg <| "This file could not be found on the server. Maybe it expired a long time ago?"

        NotFound ->
            viewErrorMsg <| "I couldn't find the page you tried to reach!"


viewErrorMsg : String -> Html Msg
viewErrorMsg msg =
    div [ class "container", class "error" ]
        [ h1 [] [ text ":(" ]
        , p [] [ text msg ]
        , a [ onClick GoHome ] [ text "Home Page" ]
        ]
