module Page.Share exposing (Model, Msg, init, update, view)

import Browser exposing (Document)
import Browser.Navigation as Nav
import FileId exposing (FileId)
import Html exposing (..)
import Html.Attributes exposing (class, disabled)
import Html.Events exposing (onClick)
import Page.Error exposing (Error(..))
import Ports exposing (copyToClipboard)
import Process exposing (sleep)
import Request as Request exposing (MetaFile)
import Route as Route exposing (Route(..))
import Task as Task exposing (Task, perform)
import Url exposing (Url)


type alias LoadedModel =
    { file : MetaFile
    , baseUrl : Url
    , copied : Bool
    }


type Model
    = Loading Url FileId
    | Loaded LoadedModel


type Msg
    = MetaFileLoaded (Result Error MetaFile)
    | Uncopy
    | Share
    | GotoFile
    | GotoHome


init : Url -> FileId -> ( Model, Cmd Msg )
init url id =
    ( Loading url id, Request.getMetaFile MetaFileLoaded id )


update : Nav.Key -> Msg -> Model -> Result Error ( Model, Cmd Msg )
update key msg model =
    case ( msg, model ) of
        ( MetaFileLoaded (Ok metaFile), Loading url id ) ->
            let
                newModel =
                    { file = metaFile
                    , baseUrl = url
                    , copied = False
                    }
            in
            Ok ( Loaded newModel, Cmd.none )

        ( MetaFileLoaded (Err err), _ ) ->
            Err err

        ( Share, Loaded loaded ) ->
            let
                newModel =
                    Loaded { loaded | copied = True }

                cmds =
                    [ copyToClipboard <| Route.share loaded.baseUrl (NamedFile loaded.file.id loaded.file.filename)
                    , perform (\_ -> Uncopy) <| sleep 3000
                    ]
            in
            Ok ( newModel, Cmd.batch cmds )

        ( Uncopy, Loaded loaded ) ->
            Ok ( Loaded { loaded | copied = False }, Cmd.none )

        ( GotoFile, Loaded loaded ) ->
            Ok ( model, Route.pushUrl key (NamedFile loaded.file.id loaded.file.filename) )

        ( GotoHome, Loaded loaded ) ->
            Ok ( model, Route.pushUrl key Home )

        ( _, _ ) ->
            Ok ( model, Cmd.none )


view : Model -> Document Msg
view model =
    case model of
        Loading _ _ ->
            Document "Loading..." []

        Loaded loaded ->
            { title = "Share " ++ loaded.file.filename
            , body = [ viewLoaded loaded ]
            }


viewLoaded : LoadedModel -> Html Msg
viewLoaded loaded =
    let
        btnLabel =
            if loaded.copied then
                "Copied to Clipboard!"

            else
                "Share!"
    in
    div [ class "container", class "finished" ]
        [ h1 [] [ i [ class "i-checkmark" ] [] ]
        , p [] [ text <| loaded.file.filename ++ " was successfully uploaded!" ]
        , viewButtons loaded.copied
        ]


viewButtons : Bool -> Html Msg
viewButtons copied =
    if copied then
        div [ class "actions" ] [ button [ disabled True ] [ text <| "Copied to clipboard!" ] ]

    else
        div [ class "actions" ]
            [ button [ onClick GotoHome ] [ text "Another!" ]
            , button [ onClick Share ] [ text "Share!" ]
            , button [ onClick GotoFile ] [ text "Open!" ]
            ]
