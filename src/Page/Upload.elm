module Page.Upload exposing (Model, Msg, init, subscriptions, update, view)

import Browser exposing (Document)
import Browser.Navigation as Nav
import FileHandle exposing (FileHandle, onDragEnter, onDragLeave, onDragOver, onDrop)
import Filesize as Filesize
import Flags exposing (Flags)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Page.Error exposing (Error(..))
import Request as Request exposing (MetaFile, UploadFileProgress(..), uploadFile, uploadFileProgress)
import Route as Route


type alias UploadingModel =
    { file : FileHandle
    , progress : Maybe ( Int, Int )
    }


type Model
    = Waiting Bool
    | Uploading UploadingModel


type Msg
    = DragEnter
    | DragExit
    | Drop FileHandle
    | AgbAccepted Bool
    | Progress UploadFileProgress


init : Model
init =
    Waiting False


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Waiting _ ->
            Sub.none

        Uploading { file, progress } ->
            uploadFileProgress Progress file


update : Nav.Key -> Msg -> Flags -> Model -> Result Error ( Flags, Model, Cmd Msg )
update key msg flags model =
    case msg of
        DragEnter ->
            Ok ( flags, Waiting True, Cmd.none )

        DragExit ->
            Ok ( flags, Waiting False, Cmd.none )

        Drop file ->
            let
                -- @Hardcoded
                maxSize =
                    100 * 1024 * 1024
            in
            if not flags.uploadAccepted then
                Ok ( flags, Waiting False, Cmd.none )

            else if FileHandle.isDirectory file then
                Err DirectoryDropped

            else if file.size < maxSize then
                Ok ( flags, Uploading { file = file, progress = Nothing }, uploadFile file )

            else
                Err (FileToBig { size = file.size, max = maxSize })

        AgbAccepted accepted ->
            Ok ( { flags | uploadAccepted = accepted }, model, Cmd.none )

        Progress (InProgress file progress) ->
            Ok ( flags, Uploading { file = file, progress = Just progress }, Cmd.none )

        Progress (Completed metaFile) ->
            Ok ( flags, model, Route.pushUrl key (Route.Share metaFile.id metaFile.filename) )

        Progress (Failed err) ->
            Err err


view : Flags -> Model -> Document Msg
view flags model =
    case model of
        Waiting dragging ->
            Document "Share It!" [ viewWaiting flags dragging ]

        Uploading uploadingModel ->
            viewUploading uploadingModel


viewWaiting : Flags -> Bool -> Html Msg
viewWaiting flags dragging =
    let
        attrs =
            [ class "container"
            , class "dropzone"
            , onDragEnter DragEnter
            , onDragLeave DragExit
            , onDrop Drop
            ]

        dropText =
            if flags.uploadAccepted then
                "Drop it!"
            else
                "Please accept our conditions first!"
    in
    if dragging then
        div (class "dragging" :: attrs)
            [ h1 [] [ i [ class "i-arrow-thick-down" ] [] ]
            , p [] [ text dropText ]
            ]

    else
        div attrs
            [ h1 [] [ i [ class "i-arrow-thick-down" ] [] ]
            , p [] [ text "Drop a file here to upload it!" ]
            , viewCheckbox "agb" AgbAccepted "I understand that I'm about to upload files to a random computer on the internet" flags.uploadAccepted
            ]


viewCheckbox : String -> (Bool -> msg) -> String -> Bool -> Html msg
viewCheckbox theId toMsg theLabel state =
    div [ class "checkbox" ]
        [ input [ id theId, type_ "checkbox", checked state, onCheck toMsg ] []
        , label [ for theId ] [ text theLabel ]
        ]


viewUploading : UploadingModel -> Document Msg
viewUploading model =
    let
        percentage =
            model.progress
                |> Maybe.map (\( bytes, bytesTotal ) -> toFloat bytes / toFloat bytesTotal * 100)
                |> Maybe.withDefault 0.0

        percentageStr =
            String.fromInt (round percentage) ++ "%"

        percentageBg =
            "linear-gradient(90deg, black 0%, black " ++ String.fromFloat percentage ++ "%, white " ++ String.fromFloat percentage ++ "%, white 100%)"

        sizeStr =
            model.progress
                |> Maybe.map (\( bytes, bytesTotal ) -> Filesize.format bytes ++ " / " ++ Filesize.format bytesTotal)
                |> Maybe.withDefault ""
    in
    { title = percentageStr ++ " Uploading"
    , body =
        [ div [ class "progress", style "background" percentageBg ]
            [ div [ class "container" ]
                [ h1 [] [ text percentageStr ]
                , p [] [ text <| "Uploading " ++ model.file.name ]
                , small [] [ text sizeStr ]
                ]
            ]
        ]
    }
