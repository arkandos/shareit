module Page.View exposing (Model, Msg, init, update, view)

import Browser exposing (Document)
import Browser.Navigation as Nav
import FileId exposing (FileId)
import Filesize as Filesize
import Flags exposing (Flags)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onCheck, onClick)
import Page.Error exposing (Error(..))
import Process exposing (sleep)
import Request exposing (MetaFile)
import Route as Route exposing (Route(..))
import Task as Task exposing (Task)
import Time as Time exposing (Month(..), Posix, Weekday(..))



---
--- MODEL
---


type Msg
    = TimeZoneLoaded Time.Zone
    | MetaFileLoaded (Result Error MetaFile)
    | Timer
    | AgbAccepted Bool
    | StartDownload


type alias LoadingModel =
    { id : FileId
    , name : Maybe String
    }


type alias LoadedModel =
    { zone : Time.Zone
    , file : MetaFile
    , timer : Int
    }


type Model
    = Loading LoadingModel
    | PartiallyLoaded1 LoadingModel MetaFile
    | PartiallyLoaded2 LoadingModel Time.Zone
    | Loaded LoadedModel


init : FileId -> Maybe String -> ( Model, Cmd Msg )
init id name =
    ( Loading (LoadingModel id name)
    , Cmd.batch
        [ Task.perform TimeZoneLoaded Time.here
        , Request.getMetaFile MetaFileLoaded id
        ]
    )



---
--- UPDATE
---


update : Nav.Key -> Msg -> Flags -> Model -> Result Error ( Flags, Model, Cmd Msg )
update key msg flags model =
    case ( msg, model ) of
        -- Nothing Loaded -> Partially Loaded
        ( TimeZoneLoaded zone, Loading loading ) ->
            Ok ( flags, PartiallyLoaded2 loading zone, Cmd.none )

        ( MetaFileLoaded (Ok metaFile), Loading loading ) ->
            Ok ( flags, PartiallyLoaded1 loading metaFile, Cmd.none )

        -- Partially Loaded -> Loaded
        ( TimeZoneLoaded zone, PartiallyLoaded1 loading file ) ->
            Ok (updateLoaded key flags loading zone file)

        ( MetaFileLoaded (Ok metaFile), PartiallyLoaded2 loading zone ) ->
            Ok (updateLoaded key flags loading zone metaFile)

        ( MetaFileLoaded (Err err), _ ) ->
            Err err

        ( Timer, Loaded loaded ) ->
            let
                newModel =
                    { loaded | timer = loaded.timer - 1 }
            in
            Ok ( flags, Loaded newModel, timer newModel )

        ( AgbAccepted accepted, _ ) ->
            Ok ( { flags | downloadAccepted = accepted }, model, Cmd.none )

        ( StartDownload, Loaded loaded ) ->
            Ok ( flags, model, Route.load (Download loaded.file.downloadId loaded.file.filename) )

        ( _, _ ) ->
            Ok ( flags, model, Cmd.none )


updateLoaded : Nav.Key -> Flags -> LoadingModel -> Time.Zone -> MetaFile -> ( Flags, Model, Cmd Msg )
updateLoaded key flags loading zone metaFile =
    if Maybe.withDefault "" loading.name == metaFile.filename then
        let
            model =
                { zone = zone
                , file = metaFile
                , timer = 3
                }
        in
        ( flags, Loaded model, timer model )

    else
        ( flags, Loading loading, Route.replaceUrl key (Route.NamedFile metaFile.id metaFile.filename) )


timer : LoadedModel -> Cmd Msg
timer model =
    if model.timer > 0 then
        Task.perform (\_ -> Timer) (sleep 1000)

    else
        Cmd.none



---
--- VIEW
---


view : Flags -> Model -> Document Msg
view flags model =
    case model of
        Loaded loaded ->
            { title = loaded.file.filename ++ " - Share It!"
            , body = [ viewLoaded flags loaded ]
            }

        _ ->
            Document "Loading..." []


viewLoaded : Flags -> LoadedModel -> Html Msg
viewLoaded flags model =
    let
        download =
            if model.timer == 0 && flags.downloadAccepted then
                a [ onClick StartDownload ] [ text model.file.filename ]

            else if model.timer > 0 then
                span [] [ text <| model.file.filename ++ " (" ++ String.fromInt model.timer ++ ")" ]

            else
                span [] [ text model.file.filename ]
    in
    div [ class "container", class "file" ]
        [ h1 [] [ i [ class "i-doc-inv" ] [] ]
        , download
        , viewCheckbox "agb" AgbAccepted "I understand that I will be downloading files from a random computer on the internet" flags.downloadAccepted
        , p []
            [ text <| Filesize.format model.file.size
            , br [] []
            , small [] [ text <| "Uploaded on " ++ timeToString model.zone model.file.created ]
            ]
        ]


viewCheckbox : String -> (Bool -> msg) -> String -> Bool -> Html msg
viewCheckbox theId toMsg theLabel state =
    div [ class "checkbox" ]
        [ input [ id theId, type_ "checkbox", checked state, onCheck toMsg ] []
        , label [ for theId ] [ text theLabel ]
        ]


timeToString : Time.Zone -> Posix -> String
timeToString zone timestamp =
    let
        hour =
            String.fromInt <| Time.toHour zone timestamp

        minute =
            String.fromInt <| Time.toMinute zone timestamp

        day =
            String.fromInt <| Time.toDay zone timestamp

        month =
            monthToString <| Time.toMonth zone timestamp

        year =
            String.fromInt <| Time.toYear zone timestamp
    in
    day ++ ". " ++ month ++ " " ++ year ++ ", " ++ hour ++ ":" ++ minute


monthToString : Month -> String
monthToString month =
    case month of
        Jan ->
            "January"

        Feb ->
            "February"

        Mar ->
            "March"

        Apr ->
            "April"

        May ->
            "May"

        Jun ->
            "June"

        Jul ->
            "July"

        Aug ->
            "August"

        Sep ->
            "September"

        Oct ->
            "October"

        Nov ->
            "November"

        Dec ->
            "December"
