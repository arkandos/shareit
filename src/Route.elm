module Route exposing (Route(..), href, load, parse, pushUrl, replaceUrl, share)

import Browser.Navigation as Nav
import FileId as FileId exposing (FileId)
import Html exposing (Attribute)
import Html.Attributes as Attr
import Url exposing (Protocol(..), Url)
import Url.Builder as Builder exposing (absolute)
import Url.Parser as Parser exposing ((</>), Parser, map, oneOf, s, string, top)


type Route
    = Home
    | File FileId
    | NamedFile FileId String
    | Share FileId String
    | Download String String
    | Impressum
    | NotFound


parser : Parser (Route -> a) a
parser =
    oneOf
        [ map Impressum <| s "impressum"
        , map NotFound <| s "404"
        , map Download <| s "download" </> string </> decodedString
        , map Share <| s "share" </> FileId.urlParser </> decodedString
        , map NamedFile <| FileId.urlParser </> decodedString
        , map File <| FileId.urlParser
        , map Home <| top
        ]


share : Url -> Route -> String
share baseUrl route =
    let
        protocol =
            case baseUrl.protocol of
                Http ->
                    "http://"

                Https ->
                    "https://"

        port_ =
            baseUrl.port_
                |> Maybe.map String.fromInt
                |> Maybe.map ((++) ":")
                |> Maybe.withDefault ""
    in
    protocol ++ baseUrl.host ++ port_ ++ toString route


parse : Url -> Maybe Route
parse url =
    Parser.parse parser url


href : Route -> Attribute msg
href targetRoute =
    Attr.href (toString targetRoute)


load : Route -> Cmd msg
load route =
    Nav.load (toString route)


replaceUrl : Nav.Key -> Route -> Cmd msg
replaceUrl key route =
    Nav.replaceUrl key (toString route)


pushUrl : Nav.Key -> Route -> Cmd msg
pushUrl key route =
    Nav.pushUrl key (toString route)


toString : Route -> String
toString route =
    case route of
        Home ->
            absolute [] []

        File id ->
            absolute [ FileId.toString id ] []

        NamedFile id name ->
            absolute [ FileId.toString id, name ] []

        Share id name ->
            absolute [ "share", FileId.toString id, name ] []

        Download id name ->
            absolute [ "download", id, name ] []

        Impressum ->
            absolute [ "impressum" ] []

        NotFound ->
            absolute [ "404" ] []



-- For some reason, Url.Parser.string does not decode the value
-- I think it should, and there is already a ticket describing this exact problem:
-- https://github.com/elm/url/issues/16
--
-- For now, we can create a custom parser handling this issue.


decodedString : Parser (String -> a) a
decodedString =
    Parser.custom "STRING" Url.percentDecode
