module Main exposing (main)

import Browser exposing (Document, UrlRequest(..), application)
import Browser.Navigation as Nav
import Flags as Flags exposing (Flags)
import Html as Html
import Json.Encode exposing (Value)
import Page.Error as Error exposing (Error(..))
import Page.Share as Share
import Page.Upload as Upload
import Page.View as View
import Route as Route exposing (Route(..))
import Url exposing (Url)


main : Program Value Model Msg
main =
    application
        { view = view
        , update = update
        , subscriptions = subscriptions
        , init = init
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }


---
--- MODEL
---


type Page
    = Upload Upload.Model
    | View View.Model
    | Share Share.Model
    | Error Error


type alias Model =
    { flags : Flags
    , key : Nav.Key
    , url : Url
    , page : Page
    }


type Msg
    = NoOp
    | LinkClicked UrlRequest
    | UrlChanged Url
    | GotUploadMsg Upload.Msg
    | GotViewMsg View.Msg
    | GotShareMsg Share.Msg
    | GotErrorMsg Error.Msg


init : Value -> Url -> Nav.Key -> ( Model, Cmd Msg )
init jsonFlags url key =
    let
        flags =
            Flags.decode jsonFlags

        model =
            { flags = flags
            , key = key
            , url = url
            , page = Error Error.NotFound
            }
    in
    changeRouteTo url (Route.parse url |> Maybe.withDefault Home) model


subscriptions : Model -> Sub Msg
subscriptions model =
    case model.page of
        Upload upload ->
            Sub.map GotUploadMsg (Upload.subscriptions upload)

        _ ->
            Sub.none


---
--- UPDATE
---


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model.page ) of
        ( LinkClicked (Internal url), _ ) ->
            case Route.parse url of
                Just (Download id name) ->
                    -- Download files are internal links, but should not
                    -- be handled by the internal SPA router.
                    -- Instead, we detect them here an let the browser handle them.
                    ( model, Route.load (Download id name) )

                Just route ->
                    ( model, Route.pushUrl model.key route )

                Nothing ->
                    -- Not an internal route we know of - ignore
                    ( model, Cmd.none )

        ( LinkClicked (External url), _ ) ->
            ( model, Nav.load url )

        ( UrlChanged url, _ ) ->
            let
                modelWithUrl =
                    { model | url = url }
            in
            case Route.parse url of
                Just route ->
                    changeRouteTo url route modelWithUrl

                Nothing ->
                    ( modelWithUrl, Route.replaceUrl model.key Route.NotFound )

        ( GotUploadMsg uploadMsg, Upload upload ) ->
            Upload.update model.key uploadMsg model.flags upload
                |> updateWithFlags Upload GotUploadMsg model

        ( GotViewMsg viewMsg, View viewModel ) ->
            View.update model.key viewMsg model.flags viewModel
                |> updateWithFlags View GotViewMsg model

        ( GotShareMsg shareMsg, Share shareModel ) ->
            Share.update model.key shareMsg shareModel
                |> updateWith Share GotShareMsg model

        ( GotErrorMsg errorMsg, Error error ) ->
            ( model, Error.update model.key errorMsg )

        ( _, _ ) ->
            -- Ignore messages for the wrong pages.
            ( model, Cmd.none )


changeRouteTo : Url -> Route -> Model -> ( Model, Cmd Msg )
changeRouteTo url route model =
    case route of
        Home ->
            Ok (none Upload.init)
                |> updateWith Upload GotUploadMsg model

        File id ->
            Ok (View.init id Nothing)
                |> updateWith View GotViewMsg model

        NamedFile id name ->
            Ok (View.init id (Just name))
                |> updateWith View GotViewMsg model

        Route.Share id name ->
            Ok (Share.init url id)
                |> updateWith Share GotShareMsg model

        _ ->
            ( model, Cmd.none )


none : model -> ( model, Cmd msg )
none model =
    ( model, Cmd.none )


updateWith : (pageModel -> Page) -> (pageMsg -> Msg) -> Model -> Result Error ( pageModel, Cmd pageMsg ) -> ( Model, Cmd Msg )
updateWith toPage toMsg model pageUpdateResult =
    case pageUpdateResult of
        Ok ( pageModel, pageMsg ) ->
            ( { model | page = toPage pageModel }
            , Cmd.map toMsg pageMsg
            )

        Err err ->
            ( { model | page = Error err }, Cmd.none )


updateWithFlags : (pageModel -> Page) -> (pageMsg -> Msg) -> Model -> Result Error ( Flags, pageModel, Cmd pageMsg ) -> ( Model, Cmd Msg )
updateWithFlags toPage toMsg model pageUpdateResult =
    case pageUpdateResult of
        Ok ( newFlags, pageModel, pageMsg ) ->
            ( { model
                | page = toPage pageModel
                , flags = newFlags
              }
            , Cmd.batch
                [ Cmd.map toMsg pageMsg
                , Flags.store newFlags
                ]
            )

        Err err ->
            ( { model | page = Error err }, Cmd.none )


---
--- VIEW
---


view : Model -> Document Msg
view model =
    case model.page of
        Upload upload ->
            viewPage (Upload.view model.flags) GotUploadMsg upload

        View viewModel ->
            viewPage (View.view model.flags) GotViewMsg viewModel

        Share shareModel ->
            viewPage Share.view GotShareMsg shareModel

        Error err ->
            viewPage Error.view (\_ -> NoOp) err


viewPage : (pageModel -> Document pageMsg) -> (pageMsg -> Msg) -> pageModel -> Document Msg
viewPage pageView toMsg model =
    let
        liftBody =
            List.map (Html.map toMsg)

        { title, body } =
            pageView model
    in
    Document title (liftBody body)
