port module Ports exposing (copyToClipboard, persistFlags, progressDecoder, uploadFile, uploadFileProgress)

import Http exposing (Error(..), Response)
import Http.Progress exposing (Progress(..))
import Json.Decode as D
import Json.Encode exposing (Value)


type alias Request =
    { method : String
    , headers : List ( String, String )
    , url : String
    , body : Value
    , timeout : Maybe Float
    , withCredentials : Bool
    }


responseDecoder : D.Decoder body -> D.Decoder (Response body)
responseDecoder bodyDecoder =
    D.map4 Response
        (D.field "url" D.string)
        (D.field "status" <|
            D.map2 (\code msg -> { code = code, message = msg })
                (D.field "code" D.int)
                (D.field "message" D.string)
        )
        (D.field "headers" <| D.dict D.string)
        (D.field "body" bodyDecoder)


progressDecoder : D.Decoder value -> D.Decoder (Progress value)
progressDecoder valueDecoder =
    D.andThen
        (\tag ->
            case tag of
                "none" ->
                    D.succeed None

                "some" ->
                    D.map2 (\bytes bytesExpected -> Some { bytes = bytes, bytesExpected = bytesExpected })
                        (D.field "bytes" D.int)
                        (D.field "bytesExpected" D.int)

                "fail" ->
                    D.field "error" D.string
                        |> D.andThen
                            (\error ->
                                case error of
                                    "badUrl" ->
                                        D.field "reason" D.string |> D.map BadUrl

                                    "timeout" ->
                                        D.succeed Timeout

                                    "networkError" ->
                                        D.succeed NetworkError

                                    "badStatus" ->
                                        D.field "response" (responseDecoder D.string)
                                            |> D.map BadStatus

                                    "badPayload" ->
                                        D.map2 BadPayload
                                            (D.field "reason" D.string)
                                            (D.field "response" (responseDecoder D.string))

                                    _ ->
                                        D.fail <| "Unexpected Error '" ++ error ++ "'"
                            )
                        |> D.map Fail

                "done" ->
                    D.map Done <| D.field "data" valueDecoder

                _ ->
                    D.fail <| "Unexpected Progress type tag '" ++ tag ++ "'"
        )
        (D.field "tag" D.string)


port uploadFile : Request -> Cmd msg


port uploadFileProgress : (Value -> msg) -> Sub msg


port persistFlags : Value -> Cmd msg


port copyToClipboard : String -> Cmd msg
