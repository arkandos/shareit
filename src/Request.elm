module Request exposing (MetaFile, UploadFileProgress(..), getMetaFile, uploadFile, uploadFileProgress)

import FileHandle exposing (FileHandle)
import FileId as FileId exposing (FileId)
import Http as Http exposing (Request)
import Http.Progress exposing (Progress(..))
import Json.Decode as D
import Page.Error exposing (Error(..))
import Ports as P
import Time exposing (Posix, millisToPosix)
import Url.Builder exposing (absolute)


type alias MetaFile =
    { id : FileId
    , downloadId : String
    , filename : String
    , encoding : String
    , mime : String
    , size : Int
    , created : Posix
    }


type UploadFileProgress
    = InProgress FileHandle ( Int, Int )
    | Completed MetaFile
    | Failed Error


uploadFile : FileHandle -> Cmd msg
uploadFile file =
    let
        request =
            { method = "POST"
            , headers = []
            , url = "/file"
            , body = file.handle
            , timeout = Nothing
            , withCredentials = False
            }
    in
    P.uploadFile request


uploadFileProgress : (UploadFileProgress -> msg) -> FileHandle -> Sub msg
uploadFileProgress msg file =
    P.uploadFileProgress identity
        |> Sub.map (decodeUploadProgress file)
        |> Sub.map msg


getMetaFile : (Result Error MetaFile -> msg) -> FileId -> Cmd msg
getMetaFile msg fileId =
    Http.get (absolute [ "file", FileId.toString fileId ] []) metaFileDecoder
        |> Http.send identity
        |> Cmd.map (Result.mapError (mapHttpError <| Download fileId))
        |> Cmd.map msg



---
--- Internal
---


type ErrorInformation
    = Upload FileHandle
    | Download FileId


metaFileDecoder : D.Decoder MetaFile
metaFileDecoder =
    D.map7 MetaFile
        (D.field "_id" FileId.decoder)
        (D.field "fileId" D.string)
        (D.field "filename" D.string)
        (D.field "encoding" D.string)
        (D.field "mime" D.string)
        (D.field "size" D.int)
        (D.field "created" D.int |> D.map millisToPosix)


metaFileProgressDecoder : D.Decoder (Progress MetaFile)
metaFileProgressDecoder =
    P.progressDecoder metaFileDecoder


decodeUploadProgress : FileHandle -> D.Value -> UploadFileProgress
decodeUploadProgress file progressValue =
    case D.decodeValue metaFileProgressDecoder progressValue of
        Ok None ->
            InProgress file ( 0, file.size )

        Ok (Some { bytes, bytesExpected }) ->
            let
                actualExpected =
                    if bytesExpected > 0 then
                        bytesExpected

                    else
                        file.size
            in
            InProgress file ( bytes, actualExpected )

        Ok (Done metaFile) ->
            Completed metaFile

        Ok (Fail err) ->
            Failed <| mapHttpError (Upload file) err

        _ ->
            Failed UnexpectedServerError


mapHttpError : ErrorInformation -> Http.Error -> Error
mapHttpError info error =
    case error of
        Http.BadStatus response ->
            case ( response.status.code, info ) of
                ( 404, Download id ) ->
                    FileNotFound id

                ( 410, Download id ) ->
                    FileExpired id

                ( 413, Upload file ) ->
                    -- @Hardcode
                    FileToBig { size = file.size, max = 100 * 1024 * 1024 }

                ( 429, Upload file ) ->
                    -- @Hardcoded
                    QuotaReached { max = 10 }

                _ ->
                    UnexpectedServerError

        _ ->
            UnexpectedServerError
