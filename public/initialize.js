;window.addEventListener("load", function() {
    function disableEvent(ev) {
        ev.preventDefault();
        ev.stopPropagation();
        return false;
    }

    document.addEventListener("dragover", disableEvent);
    document.addEventListener("drop", disableEvent);


    var flags = JSON.parse(this.localStorage.getItem('flags'))

    var app = Elm.Main.init({
        flags: flags,
        node: document.getElementById('app')
    })

    app.ports.persistFlags && app.ports.persistFlags.subscribe(function (flags) {
        localStorage.setItem('flags', JSON.stringify(flags))
    })

    app.ports.uploadFile && app.ports.uploadFile.subscribe(function (request) {

        var xhr = new XMLHttpRequest();

        xhr.addEventListener('error', function () {
            app.ports.uploadFileProgress && app.ports.uploadFileProgress.send(Progress$Fail$NetworkError())
        })

        xhr.addEventListener('timeout', function () {
            app.ports.uploadFileProgress && app.ports.uploadFileProgress.send(Progress$Fail$Timeout())
        })

        xhr.upload.addEventListener('progress', function (event) {
            const len = event.lengthComputable ? event.total : request.body.size
            app.ports.uploadFileProgress && app.ports.uploadFileProgress.send(Progress$Some(event.loaded, len))
        })

        xhr.addEventListener('load', function () {
            var response = xhrToResponse(xhr)

            if(response.status.code < 200 || 300 <= response.status.code) {
                app.ports.uploadFileProgress && app.ports.uploadFileProgress.send(Progress$Fail$BadStatus(response))
                return
            }

            try {
                response.body = JSON.parse(response.body)
            } catch(e) {
                app.ports.uploadFileProgress && app.ports.uploadFileProgress.send(Progress$Fail$BadPayload(''+e, response))
                return
            }

            app.ports.uploadFileProgress && app.ports.uploadFileProgress.send(Progress$Done(response.body))
        })

        try {
            xhr.open(request.method, request.url)
        } catch(e) {
            app.ports.uploadFileProgress && app.ports.uploadFileProgress.send(Progress$Fail$BadUrl(request.url))
            return
        }

        request.headers.forEach(function (header) {
            xhr.setRequestHeader(header[0], header[1])
        })

        if (request.timeout) {
            xhr.timeout = request.timeout
        }

        xhr.withCredentials = request.withCredentials

        const formData = new FormData()
        formData.append('file', request.body)

        xhr.send(formData)
    })

    app.ports.copyToClipboard && app.ports.copyToClipboard.subscribe(function (text) {
        // Copied from https://stackoverflow.com/a/30810322/7329959

        var textArea = document.createElement("textarea");
        textArea.className = "copyzone";
        textArea.value = text;

        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copying text command was ' + msg);
        } catch (err) {
            console.log('Oops, unable to copy');
        }

        document.body.removeChild(textArea);
    })


    function Progress$None() {
        return ({ tag: 'none' })
    }

    function Progress$Some(bytes, bytesExpected) {
        return ({
            tag: 'some',
            bytes: bytes,
            bytesExpected: bytesExpected
        })
    }

    function Progress$Done(data) {
        return ({
            tag: 'done',
            data: data
        })
    }

    function Progress$Fail$BadUrl(reason) {
        return ({
            tag: 'fail',
            error: 'badUrl',
            reason: reason
        })
    }

    function Progress$Fail$Timeout() {
        return ({ tag: 'fail', 'error': 'timeout' })
    }

    function Progress$Fail$NetworkError() {
        return ({ tag: 'fail', error: 'networkError' })
    }

    function Progress$Fail$BadStatus(response) {
        return ({
            tag: 'fail',
            error: 'badStatus',
            response: response
        })
    }

    function Progress$Fail$BadPayload(reason, response) {
        return ({
            tag: 'fail',
            error: 'badPayload',
            reason: reason,
            response: response
        })
    }


    function xhrToResponse(xhr) {
        return ({
            url: xhr.responseURL,
            status: {
                code: xhr.status,
                message: xhr.statusText
            },
            headers: parseHeaders(xhr.getAllResponseHeaders()),
            body: xhr.responseText
        })

        function parseHeaders(headersString) {
            const lines = headersString.split('\r\n')

            const headers = {}

            for (let i = 0; i < lines.length; ++i) {
                const line = lines[i]
                const split = line.indexOf(': ')

                if (split > 0) {
                    const name = line.substr(0, split).toLowerCase()
                    const value = line.substr(split + 2)

                    if (headers.hasOwnProperty(name)) {
                        headers[name] = headers[name] + ', ' + value
                    } else {
                        headers[name] = value
                    }
                }
            }

            return headers
        }
    }
});
